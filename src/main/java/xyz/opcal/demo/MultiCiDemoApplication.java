package xyz.opcal.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiCiDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultiCiDemoApplication.class, args);
	}

}
